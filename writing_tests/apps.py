from django.apps import AppConfig


class WritingTestsConfig(AppConfig):
    name = 'writing_tests'
