from django.test import TestCase
from .models import Animal


class AnimalTestCase(TestCase):
    def setUp(self):
        Animal.objects.create(name='lion', sound='roar')
        Animal.objects.create(name='cat', sound='meow')

    def test_animal_can_speak(self):
        """
        Summary:
        ---------
        test whether the animal models speak method works


        Extended Description:
        ---------------------
        when the speak method calls it should return a string.


        Parameters:
        ----------
        self:


        Returns:
        --------
        string
                The {name} says '{sound}'
        """
        lion = Animal.objects.get(name='lion')
        cat = Animal.objects.get(name='cat')
        self.assertEqual(lion.speak(), "The lion says 'roar'")
        self.assertEqual(cat.speak(), "The cat says 'meow'")
