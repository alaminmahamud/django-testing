from django.db import models


# Create your models here.
class Animal(models.Model):
    name = models.CharField(max_length=100)
    sound = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def speak(self):
        return "The {name} says '{sound}'".format(
            name=self.name,
            sound=self.sound
        )
